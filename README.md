<h2><a href="/README.md">Main Repositories</a></h2>

<table>
    <thead>
        <tr>
<td align="center" width="16.6%"><a href="https://github.com/cs-MohamedAyman/Problem-Solving-Training/blob/master/README.md">  Problem Solving<br>Training  </a></td>
<td align="center" width="16.6%"><a href="https://github.com/cs-MohamedAyman/Data-Science-Case-Studies/blob/master/README.md"> Data Science<br>Case Studies </a></td>
<td align="center" width="16.6%"><a href="https://github.com/cs-MohamedAyman/eLearning-Platforms/blob/master/README.md">       eLearning<br>Platforms       </a></td>
<td align="center" width="16.6%"><a href="https://github.com/cs-MohamedAyman/Educational-Projects/blob/main/README.md">      Educational<br>Projects      </a></td>
<td align="center" width="16.6%"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/README.md">            Job<br>Interviews            </a></td>
<td align="center" width="16.6%"><a href="https://github.com/cs-MohamedAyman/Reference-Textbooks/blob/master/README.md">       Reference<br>Textbooks       </a></td>
        </tr>
    </thead>
    <tbody>
        <tr>
<td align="center"><a href="https://github.com/cs-MohamedAyman/Problem-Solving-Training/blob/master/README.md">
                   <img src="/repos-logos/problem-solving-training.jpg"       width="95%"></img></a></td>
<td align="center"><a href="https://github.com/cs-MohamedAyman/Data-Science-Case-Studies/blob/master/README.md">
                   <img src="/repos-logos/data-science-case-studies.jpg"      width="95%"></img></a></td>
<td align="center"><a href="https://github.com/cs-MohamedAyman/eLearning-Platforms/blob/master/README.md">
                   <img src="/repos-logos/elearning-platforms.jpg"            width="95%"></img></a></td>
<td align="center"><a href="https://github.com/cs-MohamedAyman/Educational-Projects/blob/main/README.md">
                   <img src="/repos-logos/educational-projects.jpg"           width="95%"></img></a></td>
<td align="center"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/README.md">
                   <img src="/repos-logos/job-interviews.jpg"                 width="95%"></img></a></td>
<td align="center"><a href="https://github.com/cs-MohamedAyman/Reference-Textbooks/blob/master/README.md">
                   <img src="/repos-logos/reference-textbooks.jpg"            width="95%"></img></a></td>
        </tr>
    </tbody>
</table>

<h2><a href="/README.md">Trainings for Jobs</a></h2>

<table>
    <thead>
        <tr>
<td align="center" width="17%"><a href="/Computer-Science/README.md">        Computer<br>Science        </a></td>
<td align="center" width="33%"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Computer-Science/Algorithm-Software-Engineer.md">Algorithm Software Engineer</a></td>
<td align="center" width="17%"><a href="/Software-Engineering/README.md">    Software<br>Engineering    </a></td>
<td align="center" width="33%"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Software-Engineering/Software-Developer-Engineer.md">Software Development Engineer</a></td>
        </tr>
    </thead>
    <tbody>
        <tr>
<td align="center"><a href="/Computer-Science/README.md">       <img src="/repos-logos/computer-science-department.jpg"        width="95%"></img></a></td>
<td align="center"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Computer-Science/Algorithm-Software-Engineer.md"><img src="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/logos/emp01.jpg" width="47.5%"></img></a>
                   <a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Computer-Science/Algorithm-Software-Engineer.md"><img src="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/logos/emp02.jpg" width="47.5%"></img></a></td>
<td align="center"><a href="/Software-Engineering/README.md">   <img src="/repos-logos/software-engineering-department.jpg"    width="95%"></img></a></td>
<td align="center"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Software-Engineering/Software-Developer-Engineer.md"><img src="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/logos/emp11.jpg" width="47.5%"></img></a>
                   <a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Software-Engineering/Software-Developer-Engineer.md"><img src="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/logos/emp12.jpg" width="47.5%"></img></a></td>
        </tr>
    </tbody>
    <thead>
        <tr>
<td align="center" width="17%"><a href="/Artificial-Intelligence/README.md"> Artificial<br>Intelligence </a></td>
<td align="center" width="33%"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Artificial-Intelligence/Deep-Learning-Engineer.md">Deep Learning Engineer</a></td>
<td align="center" width="17%"><a href="/Data-Science/README.md">            Data<br>Science            </a></td>
<td align="center" width="33%"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Data-Science/Data-Scientist.md">Data Scientist</a></td>
        </tr>
    </thead>
    <tbody>
        <tr>
<td align="center"><a href="/Artificial-Intelligence/README.md"><img src="/repos-logos/artificial-intelligence-department.jpg" width="95%"></img></a></td>
<td align="center"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Artificial-Intelligence/Deep-Learning-Engineer.md"><img src="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/logos/emp19.jpg" width="47.5%"></img></a>
                   <a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Artificial-Intelligence/Deep-Learning-Engineer.md"><img src="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/logos/emp20.jpg" width="47.5%"></img></a></td>
<td align="center"><a href="/Data-Science/README.md">           <img src="/repos-logos/data-science-department.jpg"            width="95%"></img></a></td>
<td align="center"><a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Data-Science/Data-Scientist.md"><img src="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/logos/emp27.jpg"                            width="47.5%"></img></a>
                   <a href="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/Data-Science/Data-Scientist.md"><img src="https://github.com/cs-MohamedAyman/Job-Interviews/blob/main/logos/emp28.jpg"                            width="47.5%"></img></a></td>
        </tr>
    </tbody>
</table>

<h2><a href="/About/README.md">About Author</a></h2>

<table>
    <tbody>
        <tr>
<td align="center" width="12.5%">Git        <br>(4 yrs)</td>
<td align="center" width="12.5%">C++        <br>(4 yrs)</td>
<td align="center" width="12.5%">Python     <br>(4 yrs)</td>
<td align="center" width="12.5%">Bash       <br>(4 yrs)</td>
<td align="center" width="12.5%">Jenkins    <br>(2 yrs)</td>
<td align="center" width="12.5%">Tensorflow <br>(2 yrs)</td>
<td align="center" width="12.5%">PyTorch    <br>(2 yrs)</td>
<td align="center" width="12.5%">SQL        <br>(2 yrs)</td>
        </tr>
        <tr>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/git.jpg"            width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/cpp.jpg"            width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/python.jpg"         width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/bash-scripting.jpg" width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/jenkins.jpg"        width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/tensorflow.jpg"     width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/pytorch.jpg"        width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/sql.jpg"            width="90%"></img></a></td>
        </tr>
    </tbody>
<!--
    <tbody>
        <tr>
<td align="center" width="12.5%">GoLang     <br>(1 yr)</td>
<td align="center" width="12.5%">Docker     <br>(1 yr)</td>
<td align="center" width="12.5%">Kubernetes <br>(1 yr)</td>
<td align="center" width="12.5%">GCP        <br>(1 yr)</td>
<td align="center" width="12.5%">AWS        <br>(1 yr)</td>
<td align="center" width="12.5%">Flask      <br>(1 yr)</td>
<td align="center" width="12.5%">FastAPI    <br>(1 yr)</td>
<td align="center" width="12.5%">No-SQL     <br>(1 yr)</td>
        </tr>
        <tr>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/golang.jpg"     width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/docker.jpg"     width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/kubernetes.jpg" width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/gcp.jpg"        width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/aws.jpg"        width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/flask.jpg"      width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/fastapi.jpg"    width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/repos-logos/nosql.jpg"      width="90%"></img></a></td>
        </tr>
    </tbody>
-->
</table>

<table>
    <tbody>
        <tr>
<td align="center" width="33%"><a href="/About/README.md">Sep 2013 - Jun 2017 <br>(4 years)</a></td>
<td align="center" width="33%"><a href="/About/README.md">Mar 2017 - Mar 2019 <br>(2 years)</a></td>
<td align="center" width="33%"><a href="/About/README.md">Mar 2019 - Mar 2022 <br>(3 years)</a></td>
        </tr>
        <tr>
<td align="center"><a href="/About/README.md"><img src="/About/Phases/phase1-.jpg" width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/About/Phases/phase2-.jpg" width="90%"></img></a></td>
<td align="center"><a href="/About/README.md"><img src="/About/Phases/phase3-.jpg" width="90%"></img></a></td>
        </tr>
    </tbody>
</table>
